<?php

	interface School{
		public function mySchool();
	}

	interface College{
		public function myCollege();
	}

	interface Versity{
		public function myVersity();
	}


	class Student implements School, College, Versity{

		public function __construct(){
			$this->mySchool();
			$this->myCollege();
			$this->myVersity();
		}

		public function mySchool(){
			echo "I am a school student.<br>";
		}

		public function myCollege(){
			echo "I am a collge student<br>";
		}

		public function myVersity(){
			echo "I am a versity student.<br>";
		}


	}

	$school = new Student();

?>