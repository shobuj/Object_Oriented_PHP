<?php 

	class BaseClase{
	

		function add(){
				$x = 1000;
		$y = 500;
		$sum = $x + $y;
		echo "Summation is = ".$sum."<br>";
		}
	}

	class child extends BaseClase{
		function sub(){
			$x = 333;
			$y = 333;
			$sub = $x - $y;
			echo "Subtraction is = ".$sub."<br>";
		}
	}

	class nestedChild extends child{
		function mul(){
			$x = 33;
			$y = 3;
			$mul = $x * $y;
			echo "Multiplication is = ".$mul."<br>";
		}
	}

	class show extends nestedChild{
		function __construct(){
			parent::add();
			parent::sub();
			parent::mul();
		}
	}

	$obj = new show();

 ?>