<?php 
	
	// store data in session variable
	session_start();

	//store data in session variable through user
	$_SESSION['user']  = $_POST['name'];
	$_SESSION['profile'] = $_POST['profile'];

	/*destroy one session variable(individual) use
	unset($_SESSION['user']);*/


 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Session variable</title>
</head>
<body>

	 <form method="post">
	 	<table>
	 		<tr>
	 			<th>Enter your name :</th>
	 			<td><input type="text" name="name"></td>
	 		</tr>
	 		<tr>
	 			<th>Enter your profile :</th>
	 			<td><input type="text" name="profile"></td>
	 		</tr>
	 		<tr>
	 			<td><input type="submit" name="submit" value="Sesstion Create"></td>
	 		</tr>
	 	</table>
	 </form>

</body>
</html>
