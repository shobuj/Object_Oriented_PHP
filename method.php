<!DOCTYPE html>
<html>
<head>
	<title>Method practice oop</title>
</head>
<body>

	<form action="" method="post">
		<table>

			<tr>
				<td>Enter the first number : </td>
				<td><input type="number" name="num1"></td>
			</tr>

			<tr>
				<td>Enter the second number : </td>
				<td><input type="number" name="num2"></td>
			</tr>

			<tr>
				<td></td>
				<td><input type="submit" name="calculation" value="Calculate"></td>
			</tr>

		</table>
	</form>


	<?php

		include('calculation.php');

		if(isset($_POST['calculation'])){
			$numOne = $_POST['num1'];
			$numTwo = $_POST['num2'];

			if(empty($numOne) && empty($numTwo)){
				echo "<span style='color:red'>Field must not be empty !</span><br>";
			}
			else{
				echo "First number is :".$numOne." and Second number is : ".$numTwo."<br>";
				$cal = new Calculator();
				$cal->sum($numOne, $numTwo);
				$cal->sub($numOne, $numTwo);
				$cal->mul($numOne, $numTwo);
				$cal->div($numOne, $numTwo);
			}
		}

	?>

</body>
</html>