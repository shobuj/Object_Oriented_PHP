<?php

	abstract class Teacher{
		public $name;
		public $age;

		public function student(){
			echo $this->name." is ".$this->age." years old<br>";
		}

		abstract public function School();
	}

	class Boy extends Teacher{
		public function describes(){
			return parent::student()." and I am a college student<br>";
		}

		public function School(){
			return "I am a School student.";
		}
	}

	$boy = new Boy();
	$boy->name = "shobuj";
	$boy->age  = "22";
	echo $boy->describes();
	echo $boy->School();

?>