<?php

	class UserData{
		public $name;
		public $id;
		const NAME = "Shobuj islam";

		public function __construct($a, $b){
			$this->name = $a;
			$this->id   = $b;
			echo "Username is {$this->name} and user id is {$this->id}";
		}

		public function display(){
			echo "Full name is ".Userdata::NAME;
		}

		public function __destruct(){
			unset($this->name);
			unset($this->id);
		}
	}

	$name = "shobuj";
	$id   = "94";
	$user = new UserData($name, $id);
	echo "<br>";
	$user->display();
?>