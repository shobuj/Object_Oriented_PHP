<?php 

	class Base{
		public $name = "shobuj";
		protected $profile = "developer";
		private $salary = 30000;

		public function show(){
			echo "Welcome ".$this->name;
			echo "Profile ".$this->profile;
			echo "Salary ".$this->salary;
		}
	}

	class child extends Base{
		public function show1(){
			echo "Welcome ".$this->name;
			echo " Profile is ".$this->profile;
			echo " Salary is ".$this->salary;
		}
	}

	$obj = new child();
	$obj->show1();
	
 ?>