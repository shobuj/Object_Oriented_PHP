<?php 

	interface Shape{
		public function calcArea();
	}

	class Circle implements Shape{
		private $radius;

		public function __construct($value){
			$this->radius = $value;
		}

		public function calcArea(){
			echo "Radius : ". $this->radius * $this->radius * pi()."<br>";
		}
	}

	class Rectangle implements Shape{
		private $width;
		private $height;

		public function __construct($value1, $value2){
			$this->width = $value1;
			$this->height = $value2;
		}
		public function calcArea(){
			echo "Rectangle : ".$this->width * $this->height;
		}
	}

	//create object

	$circle = new Circle(5);
	$rectangle = new Rectangle(2,4);

	// execute object

	$circle->calcArea();
	$rectangle->calcArea();


?>