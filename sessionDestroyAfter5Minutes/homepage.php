<?php 

	session_start();

	if(!isset($_SESSION['user'])){
		echo "<h3 align='center'>Please Login Again :</h3>";
		echo "<a href='index.php'>Click here to login</a>";
	}

	else{
		$now = time();
		//checking the time now when the home page starts

		if($now > $_SESSION['expire']){
			session_destroy();
			echo "<p align='center'>Your session has been expired !<a href='index.php'> Login Here</a></p>";
		}
		else{


			?>


			<!DOCTYPE html>
			<html>
			<head>
				<title>Session Destroy After 5 Minutes</title>
			</head>
			<body>
				
			<p style="background: #CCCCCC">
				Welcome <?php echo $_SESSION['user'] ?><br><br>
				<span style="float: right;"><a href="logout.php">Logout<a></span>

				<span style="color: red;text-align: center">Your session destroy after 5 minutes you will redirect on next page.</span><br><br>
				<span>If you logout before 5 minutes click logout link</span>
			</p>

			</body>
			</html>


			<?php
		}
	}

 ?>