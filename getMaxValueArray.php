<?php 

	$arr = array(22,33,44,22,11,3,5,77);

	//using library function 
	echo "maximum number is : ".max($arr);
	echo "<br>";


	// using foreach loop
	$b = 0;
	foreach($arr as $key => $val){
		if($val > $b){
			$b = $val;
		}
	}
	echo "Maximum num : ".$b;
	echo "<br>";


	// using for loop
	$x = 0;
	for($i=0; $i<count($arr); $i++){
		if($arr[$i] > $b){
			$b = $arr[$i];
		}
	}
	echo "max num : ".$b;

 ?>