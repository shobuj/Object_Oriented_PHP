<?php

	class Person{
		public $name;
		public $age;

		public function __construct($a, $b){
			$this->name = $a;
			$this->age  = $b;
		}

		public function personDetails(){
			echo "Person name is {$this->name} and person age is {$this->age}";
		}
	}

	$personName = new Person("shobuj", "22");
	$personName->personDetails();

?>