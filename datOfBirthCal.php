<!DOCTYPE html>
<html>
<head>
	<title>Date of Birth Calculation</title>
</head>
<body>

	<form method="post">
		
		<center height='333'>

		Choose Your Date of Birth
			<select name="yy">
				<option value="">Year</option>
				<?php 
					for($i=1900; $i<=2017; $i++){
						echo "<option value='$i'>$i</option>";
					}
				 ?>
			</select>

			<select name="mm">
				<option value="">Month</option>
				<?php 
					for($i=1; $i<=12; $i++){
						echo "<option value='$i'>$i</option>";
					}
				 ?>
			</select>

			<select name="dd">
				<option value="">Day</option>
				<?php 
					for($i=1; $i<=31; $i++){
						echo "<option value='$i'>$i</option>";
					}
				 ?>
			</select>

			<input type="submit" name="sub" value="Check it">

		</center>

	</form>

</body>
</html>

<?php 

	if(isset($_POST['sub'])){
		$mm = $_POST['mm'];
		$dd = $_POST['dd'];
		$yy = $_POST['yy'];

		$dob = $mm."/".$dd."/".$yy;

		$arr = explode("/", $dob);

		//date_default_time_zone_set($dob);
		$dateTs = strtotime($dob);

		$now = strtotime("today");

		if(sizeof($arr) != 3){
			die('Error : Please enter a valid date.');
		}
		/*if(!checkdate($arr[0], $arr[1], $arr[2])){
			die('Please enter a valid Date of Birth.');
		}*/
		if($dateTs >= $now){
			die('Enter Date of Birth earlier today.');
		}

		$ageDays = floor(($now - $dateTs)/86400);
		$ageYears = floor($ageDays/365);
		$ageMonths = floor(($ageDays - ($ageYears*365))/30);

		echo "<br><font  color='green' size='15'>Your are $ageYears years and $ageMonths months and $ageDays day.</font>";


	}
	
 ?>