<?php

	class UserData{
		public $name;
		public $age;

		public function __construct($a, $b){
			$this->name = $a;
			$this->age  = $b;
		}

		public function display(){
			echo "Username is {$this->name} and age is {$this->age}";
		}
	}

	class Admin extends UserData{
		public $level;
		public function display(){
		echo "Admin name is {$this->name} and age is {$this->age} and id is {$this->level}";	
	 }
   }

	$user = "shobuj";
	$id = "34";

	$userdata = new UserData($user, $id);
	$userdata->display();
	echo "<br>";

	$admin = "khan";
	$id = "23";
	$ad  = new Admin($admin, $id);
	$ad->level = "administration";
	$ad->display();


?>